# Lightbox Remastered

> This repository serve resources and examples for my [Behance portfolio](https://www.behance.net/gallery/117643605/Lightbox-Remastered).

This small redesign is a personal take on lightboxes (or popins) used in One and Neo platforms. I felt the current one was in dire need of a rework and a better UI.

The project itself is a prototype in native HTML/CSS aiming to improve the box behaviour and resizing. I worked into rearranging the content panels via Flexboxs and Grids, and redefined where content could be allowed to scroll or be static. It now takes into account the viewport and the content and try to prioritize what should be resized first. I eliminated existing empties and suboptimal areas and adjusted borders and lines to create a more easy and readable component. The component have a better hierarchical reading from top to bottom and use more width.

This work allowed me to exercise on interface design and graphical design, inspired to step in the One guidelines, and improve my HTML and CSS skills. This prototype aim to suggest improvements on the product.

* [Interactive showcase](https://khosmon.gitlab.io/Lightbox-Remastered/)  
On Firefox, you can use the Adaptative View (`Ctrl / Cmd + Shift + M`) to resize it freely.
* [Before/After comparison](https://khosmon.gitlab.io/Lightbox-Remastered/juxtaposejs/)


## Gallery

![Example of a full ranged lightbox](https://khosmon.gitlab.io/Lightbox-Remastered/readme_assets/Lightbox%201.png)

![Example of a comfty lightbox](https://khosmon.gitlab.io/Lightbox-Remastered/readme_assets/Lightbox%202.png)

![Example of a neat lightbox](https://khosmon.gitlab.io/Lightbox-Remastered/readme_assets/Lightbox%203.png)

![Example of a tight lightbox](https://khosmon.gitlab.io/Lightbox-Remastered/readme_assets/Lightbox%204.png)

![Example of a narrow lightbox](https://khosmon.gitlab.io/Lightbox-Remastered/readme_assets/Lightbox%205.png)

![Example of a cramped lightbox](https://khosmon.gitlab.io/Lightbox-Remastered/readme_assets/Lightbox%206.png)


## Credits

* Fonts: Microsoft, Apple, Graig Rozynski, Purushoth Kumar Guttula
* Originals Design, Assets, Codes, Graphics: Open Digital Education

